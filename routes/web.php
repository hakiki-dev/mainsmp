<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('/ekstrakurikuler', 'Ekstrakurikuler@list')->name('ekstrakurikuler.list');
Route::get('/guru', 'Teacher@list')->name('teacher.list');
Route::get('/gallery', 'Gallery@list')->name('gallery.list');
Route::get('/pendaftaran-siswa-baru', 'StudentRegister@register')->name('psb.register');

Route::post('/register', 'StudentRegister@sendRegister')->name('psb.store');
