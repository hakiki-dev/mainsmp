@extends('base.base-main')
@section('header') @include('base.main-header', ['selected' => 'home'])  @endsection
@section('slide') 
    @include('base.main-slide')
@stop
@section('content')
    <section id="content" style="margin-bottom: 0px;">

        <div class="content-wrap">

            <section id="section-about" class="page-section">

                <div class="container clearfix" style="margin-bottom: 10px">

                    <div class="heading-block center">
                        <h2>SMP Satya Dharma Balung</h2>
                        <span>Cerdas, Berkarakter dan Berdaya Saing</span>
                    </div>

                    <div class="col_one_third nobottommargin">
                        <div class="feature-box media-box">
                            <div class="fbox-media">
                                <img src="{{asset('feature/1.png')}}" alt="Lab Komputer">
                            </div>
                            <div class="fbox-desc">
                                <h3>Ruang Multimedia<span class="subtitle">Terlengkap dan bersih</span></h3>
                                <p>dilengkapi dengan berbagai macam aplikasi yang mampu mengembangkan skill siswa/siswi dalam mengikuti persaingan teknologi berbasis Teknologi Informasi</p>
                            </div>
                        </div>
                    </div>

                    <div class="col_one_third nobottommargin">
                        <div class="feature-box media-box">
                            <div class="fbox-media">
                                <img src="{{asset('feature/2.png')}}" alt="Baris Berbaris">
                            </div>
                            <div class="fbox-desc">
                                <h3>Baris Berbaris<span class="subtitle">Disiplin dan Kerjasama</span></h3>
                                <p>kegitan baris berbaris melatih mental siswa/siswi untuk disiplin dan berkerja sama dengan baik.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col_one_third nobottommargin col_last">
                        <div class="feature-box media-box">
                            <div class="fbox-media">
                                <img src="{{asset('feature/3.png')}}" alt="Kerjasama Ruang Guru">
                            </div>
                            <div class="fbox-desc">
                                <h3>Ruang Guru<span class="subtitle">Kerjasama</span></h3>
                                <p>Melakukan kerjasama dengan ruang guru memberikan sebuah platform bagi siswa/siswi untuk dapat belajar dimanapun dan kapanpun.</p>
                            </div>
                        </div>
                    </div>

                    <div class="clear"></div>

                </div>
                <div class="container clearfix">

                    <div class="col_one_third nobottommargin">
                        <div class="feature-box media-box">
                            <div class="fbox-media">
                                <img src="{{asset('feature/4.png')}}" alt="Cerdas dan berkarakter">
                            </div>
                            <div class="fbox-desc">
                                <h3>Dewan Guru<span class="subtitle">Cerdas dan Berkarakter</span></h3>
                                <p>Dewan guru yang mampu memberikan pengalaman mengajar yang berbeda memberikan suasana belajar mengajar yang menyenangkan.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col_one_third nobottommargin">
                        <div class="feature-box media-box">
                            <div class="fbox-media">
                                <img src="{{asset('feature/5.png')}}" alt="Kegiatan Bersama">
                            </div>
                            <div class="fbox-desc">
                                <h3>Karnaval<span class="subtitle">Kegiatan Bersama</span></h3>
                                <p>Salah satu kegiatan yang melibatkan siswa dan guru untuk menyemarakan kegiatan karnaval daerah sebagai wujud kerjasama dengan siswa</p>
                            </div>
                        </div>
                    </div>

                    <div class="col_one_third nobottommargin col_last">
                        <div class="feature-box media-box">
                            <div class="fbox-media">
                                <img src="{{asset('feature/6.png')}}" alt="Kemah Bersama">
                            </div>
                            <div class="fbox-desc">
                                <h3>Pramuka<span class="subtitle">Kemah Bersama</span></h3>
                                <p>kegiatan melatih siswa/siswi dalam menaklukan kehidupan di alam bersama teman-teman setingkat untuk melatih kerjasama dan kemampuan kepemimpinan</p>
                            </div>
                        </div>
                    </div>

                    <div class="clear"></div>

                </div>


                {{-- <div class="section dark parallax nobottommargin skrollable skrollable-between" style="padding: 80px 0px; background-image: url(&quot;images/parallax/front.jpg&quot;); background-position: 0px -65.3883px;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px -300px;">

                    <div class="container clearfix">

                        <div class="col_one_fourth nobottommargin center bounceIn animated" data-animate="bounceIn">
                            <i class="i-plain i-xlarge divcenter nobottommargin icon-code"></i>
                            <div class="counter counter-lined"><span data-from="100" data-to="846" data-refresh-interval="50" data-speed="2000">846</span>K+</div>
                            <h5>Lines of Codes</h5>
                        </div>

                        <div class="col_one_fourth nobottommargin center bounceIn animated" data-animate="bounceIn" data-delay="200">
                            <i class="i-plain i-xlarge divcenter nobottommargin icon-magic"></i>
                            <div class="counter counter-lined"><span data-from="3000" data-to="15360" data-refresh-interval="100" data-speed="2500">15360</span>+</div>
                            <h5>KBs of HTML Files</h5>
                        </div>

                        <div class="col_one_fourth nobottommargin center bounceIn animated" data-animate="bounceIn" data-delay="400">
                            <i class="i-plain i-xlarge divcenter nobottommargin icon-file-text"></i>
                            <div class="counter counter-lined"><span data-from="10" data-to="386" data-refresh-interval="25" data-speed="3500">386</span>*</div>
                            <h5>No. of Templates</h5>
                        </div>

                        <div class="col_one_fourth nobottommargin center col_last bounceIn animated" data-animate="bounceIn" data-delay="600">
                            <i class="i-plain i-xlarge divcenter nobottommargin icon-time"></i>
                            <div class="counter counter-lined"><span data-from="60" data-to="1200" data-refresh-interval="30" data-speed="2700">1200</span>+</div>
                            <h5>Hours of Coding</h5>
                        </div>

                    </div>

                </div> --}}

            </section>
            <section class="topmargin-sm">
                <div class="row  common-height">
					<div class="col-lg-4 dark col-padding ohidden" style="background-color: rgb(26, 188, 156); height: 311.4px;">
						<div>
							<h3 class="uppercase" style="font-weight: 600;">VISI</h3>
							<p style="line-height: 1.8;">Terwujudnya  Generasi yang Cerdas, Berkarakter dan Berdaya Saing Berdasarkan IPTEK dan IMTAQ  ‘ala Ahlus Sunnah Wal Jama’ah.</p>
							<i class="icon-bulb bgicon"></i>
						</div>
					</div>

					<div class="col-lg-4 dark col-padding ohidden" style="background-color: rgb(52, 73, 94); height: 311.4px;">
						<div>
							<h3 class="uppercase" style="font-weight: 600;">MISI</h3>
                            <p style="line-height: 1.8;">
                            <ol>
                                <li>Menanamkan rasa Ketuhanan kepada peserta didik pada setiap pembelajaran berlangsung.</li>
                                <li>Mewujudkan pembelajaran Aktif, Inovatif, Kreatif, Efektif, dan Menyenangkan melalui pendekatan SCIENTIFIC.</li>
                                <li>Mengembangkan potensi siswa di bidang sains, seni budaya, dan olahraga melalui kegiatan intrakurikuler dan ekstrakurikuler guna menanamkan life skill Peserta Didik.</li>
                                <li>Mengembangkan Budaya Literasi dan Penguatan Pendidikan Karakter (PPK).</li>
                                <li>Melatih belajar mandiri dari berbagai sumber belajar termasuk menggunakan dan memanfaatkan teknologi informasi.</li>
                                <li>Menyiapkan sumber daya pendidik dan tenaga kependidikan yang kompetitif dan mampu menggunakan ICT dengan baik.</li>
                                <li>Mewujudkan fasilitas (sarana-prasarana) pembelajaran yang berbasis IT, Multimedia System dan kultur sekolah yang menunjang keberhasilan pembelajaran berbasis Smartphone dan Android System.</li>
                                <li>Melaksanakan Kegiatan Keagamaan yang sudah menjadi Kebiasaan Ulama Ahlus Sunnah Wal Jama’ah.</li>
                            </ol>
                            
                            </p>
							<i class="icon-cog bgicon"></i>
						</div>
					</div>

					<div class="col-lg-4 dark col-padding ohidden" style="background-color: rgb(231, 76, 60); height: 311.4px;">
						<div>
							<h3 class="uppercase" style="font-weight: 600;">Tujuan</h3>
                            <p style="line-height: 1.8;">
                                <ol>
                                    <li>Terlaksananya program kegiatan keagamaan seperti: Sholat Dhuha dan Sholat Dhuhur Jama’ah, Pondok Ramadhan, Maulid Nabi SAW dll.</li>
                                    <li>Tersedianya media pembelajaran  standar  yang diperlukan dengan berbasis ICT dan Android</li>
                                    <li>Ter wujudnya peserta didik menjadi pribadi yang mandiri, bertanggung jawab, disiplin, berakhlak mulia, cakap, dan demokratif (Berlandaskan pendidikan Budaya dan Karakter bangsa ).</li>
                                </ol>
                                </p>
							<i class="icon-thumbs-up bgicon"></i>
						</div>
					</div>

				</div>
            </section>

        </div>

    </section>
@endsection