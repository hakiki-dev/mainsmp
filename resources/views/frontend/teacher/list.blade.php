@extends('base.base-main')
@section('header') @include('base.main-header', ['selected' => 'teacher'])  @endsection
@section('content')
    <section id="content" style="margin-bottom:0;">
        <section id="section-team" class="page-section topmargin-sm">

            <div class="heading-block center">
                <h2>Guru</h2>
                <span>Mengabdi untuk negeri, mencetak siswa/siswi unggul</span>
            </div>

            <div class="container clearfix">

                <div class="row">
                    <div class="col-lg-6 bottommargin">
                        <div class="team team-list clearfix">
                            <div class="team-image">
                                <img src="{{asset('content/teacher/masruhi.jpg')}}" alt="Drs. Masruhi Edrus">
                            </div>
                            <div class="team-desc">
                                <div class="team-title"><h4>Drs. Masruhi Edrus</h4><span>KOMITA</span></div>
                                <div class="team-content">-</div>
                                <a href="#" class="social-icon si-rounded si-small si-forrst">
                                    <i class="icon-forrst"></i>
                                    <i class="icon-forrst"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-skype">
                                    <i class="icon-skype"></i>
                                    <i class="icon-skype"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-flickr">
                                    <i class="icon-flickr"></i>
                                    <i class="icon-flickr"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 bottommargin">
                        <div class="team team-list clearfix">
                            <div class="team-image">
                                <img src="{{asset('content/teacher/amrul.jpg')}}" alt="Akhmad Hadi Karim Amrulloh, S.Pd">
                            </div>
                            <div class="team-desc">
                                <div class="team-title"><h4>Akhmad Hadi Karim Amrulloh, S.Pd</h4><span>Kepala Sekolah/PPKn</span></div>
                                <div class="team-content">melangkah maju, berbakti untuk negeri</div>
                                <a href="#" class="social-icon si-rounded si-small si-facebook">
                                    <i class="icon-facebook"></i>
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-twitter">
                                    <i class="icon-twitter"></i>
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-gplus">
                                    <i class="icon-gplus"></i>
                                    <i class="icon-gplus"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                  
                    <div class="col-lg-6 bottommargin">
                        <div class="team team-list clearfix">
                            <div class="team-image">
                                <img src="{{asset('content/teacher/ika.jpg')}}" alt="Ika Anindya Azizah, S.Pd.">
                            </div>
                            <div class="team-desc">
                                <div class="team-title"><h4>Ika Anindya Azizah, S.Pd.</h4><span>Bahasa Indonesia, Tata Boga</span></div>
                                <div class="team-content">-</div>
                                <a href="#" class="social-icon si-rounded si-small si-facebook">
                                    <i class="icon-facebook"></i>
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-twitter">
                                    <i class="icon-twitter"></i>
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-gplus">
                                    <i class="icon-gplus"></i>
                                    <i class="icon-gplus"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 bottommargin">
                        <div class="team team-list clearfix">
                            <div class="team-image">
                                <img src="{{asset('content/teacher/lukman.jpg')}}" alt="Lukman Hakim, S.Pd.">
                            </div>
                            <div class="team-desc">
                                <div class="team-title"><h4>Lukman Hakim, S.Pd.</h4><span>IPA</span></div>
                                <div class="team-content">melangkah maju, berbakti untuk negeri</div>
                                <a href="#" class="social-icon si-rounded si-small si-facebook">
                                    <i class="icon-facebook"></i>
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-twitter">
                                    <i class="icon-twitter"></i>
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-gplus">
                                    <i class="icon-gplus"></i>
                                    <i class="icon-gplus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 bottommargin">
                        <div class="team team-list clearfix">
                            <div class="team-image">
                                <img src="{{asset('content/teacher/hasun.jpg')}}" alt="Hasun Syukur, S.Pd.">
                            </div>
                            <div class="team-desc">
                                <div class="team-title"><h4>Hasun Syukur, S.Pd.</h4><span>Bahasa Inggris / Waka. Kurikulum</span></div>
                                <div class="team-content">-</div>
                                <a href="#" class="social-icon si-rounded si-small si-facebook">
                                    <i class="icon-facebook"></i>
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-twitter">
                                    <i class="icon-twitter"></i>
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-gplus">
                                    <i class="icon-gplus"></i>
                                    <i class="icon-gplus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                   
                    <div class="col-lg-6 bottommargin">
                        <div class="team team-list clearfix">
                            <div class="team-image">
                                <img src="{{asset('content/teacher/ali.jpg')}}" alt="Mohammad Ali Muhsin, S.Pd">
                            </div>
                            <div class="team-desc">
                                <div class="team-title"><h4>Mohammad Ali Muhsin, S.Pd</h4><span>IPS</span></div>
                                <div class="team-content">-</div>
                                <a href="#" class="social-icon si-rounded si-small si-facebook">
                                    <i class="icon-facebook"></i>
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-twitter">
                                    <i class="icon-twitter"></i>
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-gplus">
                                    <i class="icon-gplus"></i>
                                    <i class="icon-gplus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 bottommargin">
                        <div class="team team-list clearfix">
                            <div class="team-image">
                                <img src="{{asset('content/teacher/mahrus.jpg')}}" alt="Moh. Mahrus">
                            </div>
                            <div class="team-desc">
                                <div class="team-title"><h4>Moh. Mahrus</h4><span>Prakarya, Elektronika, Seni Budaya</span></div>
                                <div class="team-content"></div>
                                <a href="#" class="social-icon si-rounded si-small si-facebook">
                                    <i class="icon-facebook"></i>
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-twitter">
                                    <i class="icon-twitter"></i>
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-gplus">
                                    <i class="icon-gplus"></i>
                                    <i class="icon-gplus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 bottommargin">
                        <div class="team team-list clearfix">
                            <div class="team-image">
                                <img src="{{asset('content/teacher/shohibi.jpg')}}" alt="SHOHIBI, S.Pd.">
                            </div>
                            <div class="team-desc">
                                <div class="team-title"><h4>SHOHIBI, S.Pd.</h4><span>IPA</span></div>
                                <div class="team-content"></div>
                                <a href="#" class="social-icon si-rounded si-small si-facebook">
                                    <i class="icon-facebook"></i>
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-twitter">
                                    <i class="icon-twitter"></i>
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-gplus">
                                    <i class="icon-gplus"></i>
                                    <i class="icon-gplus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 bottommargin">
                        <div class="team team-list clearfix">
                            <div class="team-image">
                                <img src="{{asset('content/teacher/huda.jpg')}}" alt="Syamsul Huda, S.PdI.">
                            </div>
                            <div class="team-desc">
                                <div class="team-title"><h4>Syamsul Huda, S.PdI.</h4><span>ASWAJA</span></div>
                                <div class="team-content"></div>
                                <a href="#" class="social-icon si-rounded si-small si-facebook">
                                    <i class="icon-facebook"></i>
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-twitter">
                                    <i class="icon-twitter"></i>
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-gplus">
                                    <i class="icon-gplus"></i>
                                    <i class="icon-gplus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 bottommargin">
                        <div class="team team-list clearfix">
                            <div class="team-image">
                                <img src="{{asset('content/teacher/suyyiroh.jpg')}}" alt="Suyyiroh, S.Pd.">
                            </div>
                            <div class="team-desc">
                                <div class="team-title"><h4>Suyyiroh, S.Pd.</h4><span>Matematika</span></div>
                                <div class="team-content"></div>
                                <a href="#" class="social-icon si-rounded si-small si-facebook">
                                    <i class="icon-facebook"></i>
                                    <i class="icon-facebook"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-twitter">
                                    <i class="icon-twitter"></i>
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="#" class="social-icon si-rounded si-small si-gplus">
                                    <i class="icon-gplus"></i>
                                    <i class="icon-gplus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="w-100"></div> --}}

                    
                </div>

            </div>

            {{-- <div class="section parallax skrollable skrollable-before" style="background-image: url(&quot;images/parallax/3.jpg&quot;); padding: 100px 0px; background-position: 0px 100px;" data-bottom-top="background-position:0px 100px;" data-top-bottom="background-position:0px -100px;">
                <div class="heading-block center nobottomborder nobottommargin">
                    <h2>"Everything is designed, but some things are designed well."</h2>
                </div>
            </div> --}}

        </section>

    </section>
@endsection