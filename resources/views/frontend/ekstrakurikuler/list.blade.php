@extends('base.base-main')
@section('header') @include('base.main-header', ['selected' => 'ekstrakurikuler'])  @endsection
@section('content')
<section id="content" style="margin-bottom: 0px;">

    <div class="content">
        <div class="section nomargin">
            <div class="container clearfix">
                <div class="heading-block center nomargin">
                    <h3>Ekstrakurikuler</h3>
                </div>
            </div>
        </div>

        <div class="nomargin masonry-thumbs grid-4" data-lightbox="gallery" style="position: relative; height: 175px; width: 932.64px;">
            <a href="{{asset('content/extra/dhuha.png')}}" data-lightbox="gallery-item" style="width: 233px; position: absolute; left: 0px; top: 0px;"><img class="image_fade" src="{{asset('content/extra/dhuha.png')}}" alt="Dhuha"></a>
            <a href="{{asset('content/extra/pramuka.png')}}" data-lightbox="gallery-item" style="width: 233px; position: absolute; left: 233px; top: 0px;"><img class="image_fade" src="{{asset('content/extra/pramuka.png')}}" alt="Pramuka" style="opacity: 1;"></a>
            <a href="{{asset('content/extra/elektro.png')}}" data-lightbox="gallery-item" style="width: 233px; position: absolute; left: 466px; top: 0px;"><img class="image_fade" src="{{asset('content/extra/elektro.png')}}" alt="Elektro"></a>
            <a href="{{asset('content/extra/pencak.png')}}" data-lightbox="gallery-item" style="width: 233px; position: absolute; left: 699px; top: 0px;"><img class="image_fade" src="{{asset('content/extra/pencak.png')}}" alt="Pencak" style="opacity: 1;"></a>
        </div>

        <div class="clear"></div>

        <div class="container topmargin-lg clearfix">

            <div class="col_one_third">
                <h4>Pramuka</h4>
                <p>Kegiatan melakukan kegiatan pramuka</p>
            </div>

            <div class="col_one_third">
                <h4>Bela Diri</h4>
                <p>Hic corporis modi tempora eligendi ipsa, odit voluptatum cupiditate perferendis architecto repellat odio nam. Quas nesciunt voluptate, incidunt id dolorum corporis deleniti, sapiente a ipsam molestias accusantium.</p>
            </div>

            <div class="col_one_third">
                <h4>Madrasah Aliyah</h4>
                <p>Hic corporis modi tempora eligendi ipsa, odit voluptatum cupiditate perferendis architecto repellat odio nam. Quas nesciunt voluptate, incidunt id dolorum corporis deleniti, sapiente a ipsam molestias accusantium.</p>
            </div>

            <div class="col_one_third">
                <h4>Sholawat AL Banjari</h4>
                <p>Hic corporis modi tempora eligendi ipsa, odit voluptatum cupiditate perferendis architecto repellat odio nam. Quas nesciunt voluptate, incidunt id dolorum corporis deleniti, sapiente a ipsam molestias accusantium.</p>
            </div>
            <div class="col_one_third">
                <h4>Elektronika</h4>
                <p>Hic corporis modi tempora eligendi ipsa, odit voluptatum cupiditate perferendis architecto repellat odio nam. Quas nesciunt voluptate, incidunt id dolorum corporis deleniti, sapiente a ipsam molestias accusantium.</p>
            </div>

            <div class="col_one_third">
                <h4>Tata Boga</h4>
                <p>Hic corporis modi tempora eligendi ipsa, odit voluptatum cupiditate perferendis architecto repellat odio nam. Quas nesciunt voluptate, incidunt id dolorum corporis deleniti, sapiente a ipsam molestias accusantium.</p>
            </div>
            <div class="col_one">
                <h4>Sains</h4>
                <p>Hic corporis modi tempora eligendi ipsa, odit voluptatum cupiditate perferendis architecto repellat odio nam. Quas nesciunt voluptate, incidunt id dolorum corporis deleniti, sapiente a ipsam molestias accusantium.</p>
            </div>
            <div class="col_one">
                <h4>Komputer</h4>
                <p>Hic corporis modi tempora eligendi ipsa, odit voluptatum cupiditate perferendis architecto repellat odio nam. Quas nesciunt voluptate, incidunt id dolorum corporis deleniti, sapiente a ipsam molestias accusantium.</p>
            </div>

        </div>

    </div>

</section>
@endsection