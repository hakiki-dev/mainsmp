@extends('base.base-main')
@section('header') @include('base.main-header', ['selected' => 'psb'])  @endsection
@section('content')
    <div id="content" style="margin-bottom:0">
        <div class="content-wrap">

            <div class="container clearfix">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
            <form method="post" action="{{route('psb.store')}}">
                    @csrf
                    <div class="form-group">
                        <label for="nis">NISN</label>
                        <input type="text" class="form-control" name="nis">
                    </div>
                    
                    <div class="form-group">
                        <label for="fullname">Nama Lengkap</label>
                        <input type="text" class="form-control" name="fullname">
                    </div>
                    <div class="form-group">
                        <label for="nik">NIK</label>
                        <input type="text" class="form-control" name="nik">
                    </div>
                    <div class="form-group"> 
                        <label for="gender">Jenis Kelamin</label>
                        <select class="form-control" name="gender">
                            <option value="MALE">Laki-laki</option>
                            <option value="FEMALE">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="placebirt">Tempat Lahir</label>
                            <input type="text" class="form-control" name="placebirt">
                        </div>
                        <div class="form-group col-md-9">
                            <label for="">Tanggal lahir</label>
                            <input type="text" value="" class="form-control tleft format" placeholder="DD-MM-YYYY">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6"> 
                            <label for="fathername">Nama Ayah</label>
                            <input type="text" name="fathername" class="form-control">
                        </div>
                        <div class="form-group col-md-6"> 
                            <label for="fatherjob">Pekerjaan Ayah</label>
                            <input type="text" name="fatherjob" class="form-control">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6"> 
                            <label for="mothername">Nama Ibu</label>
                            <input type="text" name="mothername" class="form-control">
                        </div>
                        <div class="form-group col-md-6"> 
                            <label for="motherjob">Pekerjaan Ibu</label>
                            <input type="text" name="motherjob" class="form-control">
                        </div>
                    </div>
                        <div class="form-group">
                            <label for="mail">Email</label>
                            <input type="email" class="form-control" id="mail" placeholder="Email">
                        </div>
                    <div class="form-group">
                        <label for="inputAddress">Alamat Lengkap</label>
                        <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
                    </div>
                    
                    <div class="form-group">
                        <label for="inputAddress2">Nomor HP Orang Tua</label>
                        <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
                    </div>
                    <div class="form-group">
                        <label for="">Asal Sekolah</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group">
                    {!!Captcha::img()!!}<br>
                    <input type="text" class="captcha-input form-control" name="captcha" placeholder="captcha">
                    </div>
                    
                    <button type='submit' class="button button-desc button-green center  button-rounded"><div>Kirim Pendaftaran</div><span>jadi bagian dari smp satya dharma</span></button>
            </form>
            
            </div>
        </div>
    </div>
@endsection