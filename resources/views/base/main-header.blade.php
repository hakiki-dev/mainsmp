
		<!-- Header
		============================================= -->
		<header id="header" class="sticky-style-2">

			<div class="container clearfix">

				<!-- Logo
				============================================= -->
				<div id="logo" class="divcenter">
					<a href="index.html" class="standard-logo" data-dark-logo="images/logo-dark.png"><img class="divcenter" src="images/logo.png" alt="Canvas Logo"></a>
					<a href="index.html" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img class="divcenter" src="images/logo@2x.png" alt="Canvas Logo"></a>
				</div><!-- #logo end -->

			</div>

			<div id="header-wrap">

				<!-- Primary Navigation
				============================================= -->
				<nav id="primary-menu" class="style-2">

					<div class="container clearfix">

						<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

						<ul>
							<li><a href="/"><div>Beranda</div></a></li>
							<li @if ($selected === 'ekstrakurikuler') class="current" @endif><a href="{{route('ekstrakurikuler.list')}}"><div>Ektrakurikuler</div></a></li>
							<li  @if ($selected === 'teacher') class="current" @endif><a href="{{route('teacher.list')}}"><div>Guru</div></a></li>
							<li  @if ($selected === 'gallery') class="current" @endif><a href="{{route('gallery.list')}}"><div>Gallery</div></a></li>
							<li  @if ($selected === 'psb') class="current" @endif><a href="http://ppdb.smpsadharbalung.sch.id"><div>PPDB 2020</div></a></li>
						</ul>

						<!-- Top Search
						============================================= -->
						{{-- <div id="top-search">
							<a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
							<form action="search.html" method="get">
								<input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter..">
							</form>
						</div><!-- #top-search end --> --}}

					</div>

				</nav><!-- #primary-menu end -->

			</div>

		</header><!-- #header end -->