@extends('base.main')
@section('topbar')
    @include('base.main-topbar')
@stop

@section('header')
    @include('base.main-header', ['selected' => 'home'])
@stop

@section('content')
    @yield('content')
@stop



@section('footer')
    @include('base.main-footer') 
@stop
