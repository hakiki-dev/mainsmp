<!-- Footer
		============================================= -->
		<footer id="footer" class="dark">

			
			<!-- Copyrights
			============================================= -->
			<div id="copyrights">

				<div class="container clearfix">

					<div class="col_half">
						Copyrights &copy; @php echo date('Y') @endphp All Rights Reserved SMP Satya Dharma Balung | created by <a href="http://hakiki.net">hakiki.net</a><br>
						{{-- <div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div> --}}
					</div>

					<div class="col_half col_last tright">
						<div class="fright clearfix">
							<a href="https://web.facebook.com/smp.satyadharma" class="social-icon si-small si-borderless si-facebook" target="_blank">
								<i class="icon-facebook"></i>
								<i class="icon-facebook"></i>
							</a>

						</div>

						<div class="clear"></div>

						<i class="icon-envelope2"></i> info@smpsadharbalung.sch.id
					</div>

				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->