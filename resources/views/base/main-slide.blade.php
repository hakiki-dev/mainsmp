<section id="slider" class="slider-element slider-parallax swiper_wrapper clearfix slider-parallax-visible" style="height: 600px; transform: translateY(0px);">

    <div class="swiper-container swiper-parent swiper-container-horizontal swiper-container-wp8-horizontal" style="cursor: grab;">
        <div class="swiper-wrapper">
            <div class="swiper-slide dark swiper-slide-active" style="background-image: url(&quot;slider/1.png&quot;); width: 1097px;">
                <div class="container clearfix">
                    <div class="slider-caption slider-caption-center" style="top: 217px; transform: translateY(0px); opacity: 1;">
                        <h2 data-caption-animate="fadeInUp" class="fadeInUp animated">Selamat Datang di SMP Satya Dharma Balung</h2>
                        <p class="d-none d-sm-block fadeInUp animated" data-caption-animate="fadeInUp" data-caption-delay="200">Cerdas, Berkarakter dan Berdaya Saing</p>
                    </div>
                </div>
            </div>
            {{-- <div class="swiper-slide dark swiper-slide-next" style="width: 1097px;">
                <div class="container clearfix">
                    <div class="slider-caption slider-caption-center" style="top: 217px; transform: translateY(0px); opacity: 1;">
                        <h2 data-caption-animate="fadeInUp" class="not-animated">Beautifully Flexible</h2>
                        <p class="d-none d-sm-block not-animated" data-caption-animate="fadeInUp" data-caption-delay="200">Looks beautiful &amp; ultra-sharp on Retina Screen Displays. Powerful Layout with Responsive functionality that can be adapted to any screen size.</p>
                    </div>
                </div>
                <div class="video-wrap">
                    <video poster="images/videos/explore.jpg" preload="auto" loop="" autoplay="" muted="" style="width: 1097px; height: 617.067px; top: -8.5335px;">
                        <source src="images/videos/explore.mp4" type="video/mp4">
                        <source src="images/videos/explore.webm" type="video/webm">
                    </video>
                    <div class="video-overlay" style="background-color: rgba(0,0,0,0.55);"></div>
                </div>
            </div>
            <div class="swiper-slide" style="background-image: url(&quot;images/slider/swiper/3.jpg&quot;); background-position: center top; width: 1097px;">
                <div class="container clearfix">
                    <div class="slider-caption" style="top: 180px; transform: translateY(0px); opacity: 1;">
                        <h2 data-caption-animate="fadeInUp" class="not-animated">Great Performance</h2>
                        <p class="d-none d-sm-block not-animated" data-caption-animate="fadeInUp" data-caption-delay="200">You'll be surprised to see the Final Results of your Creation &amp; would crave for more.</p>
                    </div>
                </div>
            </div> --}}
        </div>
        <div class="slider-arrow-left swiper-button-disabled" style="opacity: 1;"><i class="icon-angle-left"></i></div>
        <div class="slider-arrow-right" style="opacity: 1;"><i class="icon-angle-right"></i></div>
        <div class="slide-number"><div class="slide-number-current">1</div><span>/</span><div class="slide-number-total">3</div></div>
    </div>

</section>