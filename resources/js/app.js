require('./bootstrap')

import topnavigation from './components/TopNavigation'


window.Vue = require('vue')

// Vue.component('topnavigation', require('./components/TopNavigation.vue').default)

const app = new Vue({
    el: '#app', 
    components: {
        topnavigation
    }
})