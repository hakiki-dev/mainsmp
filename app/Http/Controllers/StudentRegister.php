<?php
namespace App\Http\Controllers; 
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Validator;

class StudentRegister extends Controller { 
    function register() {
        return view('frontend.psb.register');
    }

    function sendRegister(Request $req){
        $validate = Validator::make($req->all(), [
            "captcha" => 'required|captcha'
        ], [
            'captcha' => ":attribute captcha tidak sama"
        ]);     

        if($validate->fails()){
            return redirect(route('psb.register'))
                            ->withErrors($validate)
                            ->withInput();
        }

    }
}
